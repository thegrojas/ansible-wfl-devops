<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width="30%" src="./ansible-wordmark.png" alt="Ansible Wordmark"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-TODO-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/ROLEID)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-TODO-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/PROJECTID)]() 
  [![License](https://img.shields.io/badge/license-LICENSE-blue.svg)](/LICENSE)

</div>

---

# Ansible Workflow - DevOps

This role performs the following actions:

- Installs `aws-cli` via `memiah.aws-cli` role.
- Installs `aws-vault` via `darkwizard242.awsvault` role.
- Installs `chamber`.
- [WIP] Installs `gclod`.
- [WIP] Installs `docker` and `docker-compose` via `geerlingguy.docker`.

## 🦺 Requirements

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

## 🗃️ Role Variables

*TODO*

## 📦 Dependencies

- `geerlingguy.docker` Role.
- `memiah.aws-cli` Role.
- `darkwizard242.awsvault` Role.

## 🤹 Example Playbook

*TODO*

## ⚖️ License

*TODO*

## ✍️ Author Information

*TODO*
